#include <iostream>
#include <random>
#include "List.h"


using namespace std;


int main()
{
 std::random_device rd; // will be used to seed eng1
 std::mt19937 eng1(rd()); // this engine produces different ouput every time

 float mean = 25.5f; // average
 float sd = 2.1f; // standard deviation

 std::normal_distribution<float> norm(mean,sd);

 List L1, L2;

 //Do some stuff with L1, L2, ...

    int num = norm(eng1);
    int pos = 1;

    for (int t=1; t < 10; t++)
    {
        L1.insert(num,t);
        num++;
    }

    cout<<"----------LIST1 CONTENTS----------"<<endl;
    for (int i=0; i < L1.size();i++)
    {
        cout<<L1.getAt(pos)<<" is at postion "<<pos<<endl;
        pos++;

    }


 // ...

}
